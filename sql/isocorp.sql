-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2017 at 11:31 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isocorp`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_siswa`
--

CREATE TABLE `data_siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai_1` int(11) NOT NULL,
  `nilai_2` int(11) NOT NULL,
  `nilai_3` int(11) NOT NULL,
  `disiplin_1` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disiplin_2` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nextra_1` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nextra_2` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `npribadi_1` decimal(8,2) NOT NULL,
  `npribadi_2` decimal(8,2) NOT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_siswa`
--

INSERT INTO `data_siswa` (`id`, `nis`, `nama`, `tgl_lahir`, `alamat`, `kelas`, `nilai_1`, `nilai_2`, `nilai_3`, `disiplin_1`, `disiplin_2`, `nextra_1`, `nextra_2`, `npribadi_1`, `npribadi_2`, `total`, `created_at`, `updated_at`) VALUES
(2, '222', 'Nuli Giarsyani', '2017-05-01', 'Sleman', 'X', 3, 4, 5, 'S', 'B', 'A', 'J', '4.80', '6.90', 3.68, '2017-05-10 21:18:12', '2017-05-10 21:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2017_05_10_171929_create_nilai_disiplin_table', 1),
(14, '2017_05_10_171939_create_nilai_extra_table', 1),
(15, '2017_05_10_171953_create_data_siswa_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_disiplin`
--

CREATE TABLE `nilai_disiplin` (
  `id` int(10) UNSIGNED NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nilai_disiplin`
--

INSERT INTO `nilai_disiplin` (`id`, `keterangan`, `kode`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'Sempurna', 'S', 4, '2017-05-10 21:05:47', '2017-05-10 21:05:47'),
(2, 'Baik', 'B', 3, '2017-05-10 21:05:47', '2017-05-10 21:05:47'),
(3, 'Cukup', 'C', 2, '2017-05-10 21:05:47', '2017-05-10 21:05:47'),
(4, 'Kurang', 'K', 1, '2017-05-10 21:05:47', '2017-05-10 21:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_extra`
--

CREATE TABLE `nilai_extra` (
  `id` int(10) UNSIGNED NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nilai_extra`
--

INSERT INTO `nilai_extra` (`id`, `keterangan`, `kode`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'Aktif', 'A', 3, '2017-05-10 21:05:47', '2017-05-10 21:05:47'),
(2, 'Jarang Aktif', 'J', 2, '2017-05-10 21:05:47', '2017-05-10 21:05:47'),
(3, 'Tidak Aktif', 'T', 1, '2017-05-10 21:05:47', '2017-05-10 21:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_disiplin`
--
ALTER TABLE `nilai_disiplin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_extra`
--
ALTER TABLE `nilai_extra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_siswa`
--
ALTER TABLE `data_siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `nilai_disiplin`
--
ALTER TABLE `nilai_disiplin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nilai_extra`
--
ALTER TABLE `nilai_extra`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
