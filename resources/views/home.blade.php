@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <a href="{{ url('create') }}" class="btn btn-primary">Tambah</a><br><br>
                    <table class="table table-bordered">
                        <tr>
                            <td>NIS</td>
                            <td>Nama</td>
                            <td>Alamat</td>
                            <td>Tgl Lahir</td>
                            <td>Kelas</td>
                            <td>Total Nilai</td>
                        </tr>
                        @foreach($data as $row)
                            <tr>
                                <td>{{$row->nis}}</td>
                                <td>{{$row->nama}}</td>
                                <td>{{$row->alamat}}</td>
                                <td>{{$row->tgl_lahir}}</td>
                                <td>{{$row->kelas}}</td>
                                <td>{{$row->total}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
