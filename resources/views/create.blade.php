@extends('layouts.app')

@section('footer')
<script type="text/javascript">
    $.validate({
        form : '.form-horizontal'
    });
    $('#tgl_lahir').datepicker({
      autoclose: true,
      format:"yyyy-mm-dd"
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{ url('store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nis" class="col-sm-2 control-label">NIS</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="nis" name="nis">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama" class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="nama" name="nama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="alamat" name="alamat">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kelas" class="col-sm-2 control-label">Kelas</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="kelas" name="kelas">
                            </div>
                        </div>

                        <h4>Daftar Nilai</h4>

                        <div class="form-group">
                            <label for="nilai_1" class="col-sm-2 control-label">Nilai 1</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="nilai_1" name="nilai[]" data-validation="number" data-validation-allowing="range[1;100]" data-validation-error-msg="Only allowing numbers from 1 to 100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nilai_2" class="col-sm-2 control-label">Nilai 2</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="nilai_2" name="nilai[]" data-validation="number" data-validation-allowing="range[1;100]" data-validation-error-msg="Only allowing numbers from 1 to 100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nilai_3" class="col-sm-2 control-label">Nilai 3</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="nilai_3" name="nilai[]" data-validation="number" data-validation-allowing="range[1;100]" data-validation-error-msg="Only allowing numbers from 1 to 100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="disiplin_1" class="col-sm-2 control-label">Disiplin 1</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="disiplin_1" name="disiplin[]">
                                    @foreach($disiplin as $dis)
                                        <option value="{{$dis->kode}}">{{$dis->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="disiplin_2" class="col-sm-2 control-label">Disiplin 2</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="disiplin_2" name="disiplin[]">
                                    @foreach($disiplin as $dis2)
                                        <option value="{{$dis2->kode}}">{{$dis2->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="extra_1" class="col-sm-2 control-label">Extra 1</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="extra_1" name="extra[]">
                                    @foreach($extra as $ext)
                                        <option value="{{$ext->kode}}">{{$ext->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="extra_2" class="col-sm-2 control-label">Extra 2</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="extra_2" name="extra[]">
                                    @foreach($extra as $ext2)
                                        <option value="{{$ext2->kode}}">{{$ext2->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pribadi_1" class="col-sm-2 control-label">Kepribadian 1</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="pribadi_1" name="pribadi[]" data-validation="number" data-validation-allowing="range[1;10],float" data-validation-error-msg="Only allowing numbers from 1 to 10">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pribadi_2" class="col-sm-2 control-label">Kepribadian 2</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="pribadi_2" name="pribadi[]" data-validation="number" data-validation-allowing="range[1;10],float" data-validation-error-msg="Only allowing numbers from 1 to 10">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <a href="{{ url('/') }}" class="btn btn-default">Batal</a>
                              <button type="submit" class="btn btn-default">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
