<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DisiplinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilai_disiplin')->truncate();
        DB::table('nilai_disiplin')->insert([
            [
	            'keterangan' => 'Sempurna',
	            'kode' => 'S',
	            'nilai' => 4,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
	            'keterangan' => 'Baik',
	            'kode' => 'B',
	            'nilai' => 3,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
	            'keterangan' => 'Cukup',
	            'kode' => 'C',
	            'nilai' => 2,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
	            'keterangan' => 'Kurang',
	            'kode' => 'K',
	            'nilai' => 1,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ]
        ]);
    }
}
