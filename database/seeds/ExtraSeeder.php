<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilai_extra')->truncate();
        DB::table('nilai_extra')->insert([
            [
	            'keterangan' => 'Aktif',
	            'kode' => 'A',
	            'nilai' => 3,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
	            'keterangan' => 'Jarang Aktif',
	            'kode' => 'J',
	            'nilai' => 2,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
	            'keterangan' => 'Tidak Aktif',
	            'kode' => 'T',
	            'nilai' => 1,
	            'created_at' => Carbon::now()->toDateTimeString(),
	            'updated_at' => Carbon::now()->toDateTimeString()
            ]
        ]);
    }
}
