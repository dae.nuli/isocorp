<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_siswa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nis', 25);
            $table->string('nama', 150);
            $table->date('tgl_lahir');
            $table->string('alamat', 200);
            $table->string('kelas', 80);
            $table->integer('nilai_1');
            $table->integer('nilai_2');
            $table->integer('nilai_3');
            $table->string('disiplin_1', 1);
            $table->string('disiplin_2', 1);
            $table->string('nextra_1', 1);
            $table->string('nextra_2', 1);
            $table->decimal('npribadi_1', 8, 2);
            $table->decimal('npribadi_2', 8, 2);
            $table->float('total', 8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_siswa');
    }
}
