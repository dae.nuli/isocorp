<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataSiswa;
use App\Disiplin;
use App\Extra;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Disiplin $dis, Extra $ext)
    {
        $this->dis = $dis;
        $this->ext = $ext;
    }

    public function index()
    {
        $data['data'] = DataSiswa::all();
        return view('home', $data);
    }

    public function create()
    {
        $data['disiplin'] = Disiplin::all();
        $data['extra'] = Extra::all();
        return view('create', $data);
    }

    public function store(Request $request)
    {
        $ds = new DataSiswa;
        $ds->nis = $request->nis;
        $ds->nama = $request->nama;
        $ds->tgl_lahir = $request->tgl_lahir;
        $ds->alamat = $request->alamat;
        $ds->kelas = $request->kelas;

        $ds->nilai_1 = $request->nilai[0];
        $ds->nilai_2 = $request->nilai[1];
        $ds->nilai_3 = $request->nilai[2];

        $ds->disiplin_1 = $request->disiplin[0];
        $ds->disiplin_2 = $request->disiplin[1];

        $ds->nextra_1 = $request->extra[0];
        $ds->nextra_2 = $request->extra[1];
        
        $ds->npribadi_1 = $request->pribadi[0];
        $ds->npribadi_2 = $request->pribadi[1];
        
        $nilai_disiplin_1 = $this->dis->where('kode',$request->disiplin[0])->first()->nilai;
        $nilai_disiplin_2 = $this->dis->where('kode',$request->disiplin[1])->first()->nilai;
        
        $nilai_extra_1 = $this->ext->where('kode',$request->extra[0])->first()->nilai;
        $nilai_extra_2 = $this->ext->where('kode',$request->extra[1])->first()->nilai;

        $tot_nilai = ($request->nilai[0] + $request->nilai[1] + $request->nilai[2]) / count($request->nilai);
        $tot_disiplin = ($nilai_disiplin_1 + $nilai_disiplin_2) / count($request->disiplin);
        $tot_extra = ($nilai_extra_1 + $nilai_extra_2) / count($request->extra);
        $tot_pribadi = ($request->pribadi[0] + $request->pribadi[1]) / count($request->pribadi);

        $ds->total = ($tot_nilai * (40/100)) + ($tot_disiplin * (25/100)) + ($tot_extra * (25/100)) + ($tot_pribadi * (10/100));
        $ds->save();
        return redirect('/');
    }
}
